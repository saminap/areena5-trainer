#pragma once
#include "Gladiator.hh"
#include <TCHAR.h>
#include <iostream>
#include <Windows.h>
#include <string>
#include <vector>
#include <TlHelp32.h>

// Offsets
#define BASE_OFFSET			0x001CC444
#define MONEY				0xCEC
#define GLADIATOR_LENGTH	0x138
#define FIRST_GLADIATOR		0xE32

#define STRENGTH			0x94
#define SPEED				0x96
#define MAX_MANA			0x98
#define MAX_STAMINA			0x9A
#define	STAMINA				0x9C
#define MANA				0x9E

#define FIST_OFFENSE		0x2A
#define FIST_DEFENSE		0x2C
#define SPEAR_OFFENSE		0x30
#define SPEAR_DEFENSE		0x32
#define SWORD_OFFENSE		0x36
#define SWORD_DEFENSE		0x38
#define AXE_OFFENSE			0x3C
#define AXE_DEFENSE			0x3E
#define MACE_OFFENSE		0x42
#define MACE_DEFENSE		0x44

#define SALARY				0x11E

int getBasePointer(const HANDLE &handle, DWORD baseAddress);
DWORD getModuleBaseAddress(DWORD processIdentifier, TCHAR *lpszModuleName);
DWORD getProcessId(char *processName);
HANDLE getProcessHandle(DWORD processId);
void populateGladiatorList(std::vector<Gladiator> &gladiators, DWORD basePointer, const HANDLE &handle);
void writeMemoryShortInt(const HANDLE &handle, DWORD address, short int value);
void writeMemoryInt(const HANDLE &handle, DWORD address, int value);
int readMemory(const HANDLE &handle, DWORD address);