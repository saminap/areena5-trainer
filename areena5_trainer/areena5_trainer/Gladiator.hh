#pragma once
#include <iostream>
#include <string>

class Gladiator
{
public:
	Gladiator(const std::string &name, int strength, short int speed, short int max_mana,
		short int mana, short int max_stamina, short int stamina, short int fist_offense,
		short int fist_defense, short int spear_offense, short int spear_defense,
		short int sword_offense, short int sword_defense, short int axe_offense,
		short int axe_defense, short int mace_offense, short int mace_defense, int salary);
	void printInfo() const;
	void printName() const;
	~Gladiator();

private:
	std::string name;

	short int strength;
	short int speed;
	short int max_mana;
	short int mana;
	short int max_stamina;
	short int stamina;

	short int fist_offense;
	short int fist_defense;

	short int spear_offense;
	short int spear_defense;

	short int sword_offense;
	short int sword_defense;

	short int axe_offense;
	short int axe_defense;

	short int mace_offense;
	short int mace_defense;

	int salary;
};

