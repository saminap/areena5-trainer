
#include "Gladiator.hh"
#include "MemoryHandling.hh"

#include <iostream>
#include <Windows.h>
#include <string>
#include <vector>

void showGladiators(const HANDLE &handle, DWORD basePointer);

int main(int argc, char *argv[]) {

	std::vector<Gladiator> gladiators;
	int basePointer = 0;
	std::string input = "";
	int selection = 0;
	int newValue = 0;

	DWORD processId = getProcessId("Areena 5 v1.21");
	HANDLE handle = getProcessHandle(processId);
	DWORD baseAddress = getModuleBaseAddress(processId, _T("Areena 5.exe"));

	if (baseAddress == NULL) {
		std::cerr << "Could not find window" << std::endl;
		getchar();
		return 1;
	}

	basePointer = getBasePointer(handle, baseAddress);

	while (true) {
		system("cls");
		gladiators.clear();
		populateGladiatorList(gladiators, basePointer, handle);

		if (gladiators.size() > 0) {
			std::cout << "(0) Money: " << readMemory(handle, basePointer + MONEY) << std::endl;
			std::cout << "(1) Gladiators" << std::endl;
			std::cout << std::endl;

			std::cout << "What would you like to modify? ";
			std::cin >> selection;

			if (selection == 0) {
				std::cout << std::endl << "New value: ";
				std::cin >> newValue;
				writeMemoryInt(handle, basePointer + MONEY, newValue);
			}
			else if (selection == 1) {
				int index = 0;
				std::cout << std::endl << "Gladiators: " << std::endl;
				std::vector<Gladiator>::const_iterator it = gladiators.begin();
				while (it != gladiators.end()) {
					std::cout << "(" << index++ << ") ";
					it->printName();
					++it;
				}
				std::cout << std::endl << "Which one would you like to modify? ";
				std::cin >> selection;
				gladiators.at(selection).printInfo();
				showGladiators(handle, basePointer + FIRST_GLADIATOR + selection*GLADIATOR_LENGTH);
			}
		}
		else {
			std::cout << "Could not find any gladiators" << std::endl;
			getchar();
		}
	}

	getchar();
	CloseHandle(handle);

	return 0;
}

void showGladiators(const HANDLE &handle, DWORD basePointer) {

	int newValue, selection;

	std::cout << "What would you like to modify? ";
	std::cin >> selection;

	if (selection == 0) {
		std::cout << std::endl << "New value: ";
		std::cin >> newValue;
		writeMemoryShortInt(handle, basePointer + SALARY, newValue);
	}
	else if (selection == 1) {
		std::cout << "(1) STAMINA | (2) MAX ";
		std::cin >> selection;
		std::cout << std::endl << "New value: ";
		std::cin >> newValue;
		if (selection == 1) {
			writeMemoryShortInt(handle, basePointer + STAMINA, newValue);
		}
		else if (selection == 2) {
			writeMemoryShortInt(handle, basePointer + MAX_STAMINA, newValue);
		}
	}
	else if (selection == 2) {
		std::cout << "(1) MANA | (2) MAX ";
		std::cin >> selection;
		std::cout << std::endl << "New value: ";
		std::cin >> newValue;
		if (selection == 1) {
			writeMemoryShortInt(handle, basePointer + MANA, newValue);
		}
		else if (selection == 2) {
			writeMemoryShortInt(handle, basePointer + MAX_MANA, newValue);
		}
	}
	else if (selection == 3) {
		std::cout << std::endl << "New value: ";
		std::cin >> newValue;
		writeMemoryShortInt(handle, basePointer + STRENGTH, newValue);
	}
	else if (selection == 4) {
		std::cout << std::endl << "New value: ";
		std::cin >> newValue;
		writeMemoryShortInt(handle, basePointer + SPEED, newValue);
	}
	else if (selection == 5) {
		std::cout << "(1) OFFENSE | (2) DEFENSE ";
		std::cin >> selection;
		std::cout << std::endl << "New value: ";
		std::cin >> newValue;
		if (selection == 1) {
			writeMemoryShortInt(handle, basePointer + FIST_OFFENSE, newValue);
		}
		else if (selection == 2) {
			writeMemoryShortInt(handle, basePointer + FIST_DEFENSE, newValue);
		}
	}
	else if (selection == 6) {
		std::cout << "(1) OFFENSE | (2) DEFENSE ";
		std::cin >> selection;
		std::cout << std::endl << "New value: ";
		std::cin >> newValue;
		if (selection == 1) {
			writeMemoryShortInt(handle, basePointer + SPEAR_OFFENSE, newValue);
		}
		else if (selection == 2) {
			writeMemoryShortInt(handle, basePointer + SPEAR_DEFENSE, newValue);
		}
	}
	else if (selection == 7) {
		std::cout << "(1) OFFENSE | (2) DEFENSE ";
		std::cin >> selection;
		std::cout << std::endl << "New value: ";
		std::cin >> newValue;
		if (selection == 1) {
			writeMemoryShortInt(handle, basePointer + SWORD_OFFENSE, newValue);
		}
		else if (selection == 2) {
			writeMemoryShortInt(handle, basePointer + SWORD_DEFENSE, newValue);
		}
	}
	else if (selection == 8) {
		std::cout << "(1) OFFENSE | (2) DEFENSE ";
		std::cin >> selection;
		std::cout << std::endl << "New value: ";
		std::cin >> newValue;
		if (selection == 1) {
			writeMemoryShortInt(handle, basePointer + AXE_OFFENSE, newValue);
		}
		else if (selection == 2) {
			writeMemoryShortInt(handle, basePointer + AXE_DEFENSE, newValue);
		}
	}
	else if (selection == 9) {
		std::cout << "(1) OFFENSE | (2) DEFENSE ";
		std::cin >> selection;
		std::cout << std::endl << "New value: ";
		std::cin >> newValue;
		if (selection == 1) {
			writeMemoryShortInt(handle, basePointer + MACE_OFFENSE, newValue);
		}
		else if (selection == 2) {
			writeMemoryShortInt(handle, basePointer + MACE_DEFENSE, newValue);
		}
	}
}