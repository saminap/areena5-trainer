#include "MemoryHandling.hh"

int getBasePointer(const HANDLE &handle, DWORD baseAddress) {

	int basePointer = 0;
	SIZE_T bytesRead = 0;

	ReadProcessMemory(handle, (LPCVOID)(baseAddress + BASE_OFFSET), (LPVOID)&basePointer, 4, &bytesRead);

	if (bytesRead == 0) {
		return NULL;
	}
	else {
		return basePointer;
	}
}

DWORD getModuleBaseAddress(DWORD processIdentifier, TCHAR *lpszModuleName){
	HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, processIdentifier);
	DWORD moduleBaseAddress = 0;
	if (hSnapshot != INVALID_HANDLE_VALUE)
	{
		MODULEENTRY32 ModuleEntry32 = { 0 };
		ModuleEntry32.dwSize = sizeof(MODULEENTRY32);
		if (Module32First(hSnapshot, &ModuleEntry32))
		{
			do
			{
				if (_tcscmp(ModuleEntry32.szModule, lpszModuleName) == 0)
				{
					moduleBaseAddress = (DWORD)ModuleEntry32.modBaseAddr;
					break;
				}
			} while (Module32Next(hSnapshot, &ModuleEntry32));
		}
		CloseHandle(hSnapshot);
	}
	return moduleBaseAddress;
}

DWORD getProcessId(char *processName) {

	HWND window = FindWindowA(NULL, (LPCSTR)processName);
	DWORD id;

	if (window == NULL) {
		return NULL;
	}

	GetWindowThreadProcessId(window, &id);

	return id;
}

HANDLE getProcessHandle(DWORD processId) {

	HANDLE handle = OpenProcess(PROCESS_ALL_ACCESS, false, processId);

	if (!handle) {
		return NULL;
	}

	return handle;
}

void populateGladiatorList(std::vector<Gladiator> &gladiators, DWORD basePointer, const HANDLE &handle) {
	char temp;
	int i = 0, j = 0;
	short int strength, speed, max_mana, mana, max_stamina, stamina, salary = 0;
	short int fist_offense, fist_defense;
	short int spear_offense, spear_defense;
	short int sword_offense, sword_defense;
	short int axe_offense, axe_defense;
	short int mace_offense, mace_defense;
	std::string name;
	bool nameFound;

	while (true) {
		nameFound = false;
		i = 0;
		name = "";
		while (true) {
			ReadProcessMemory(handle, (LPCVOID)(basePointer + FIRST_GLADIATOR + j*GLADIATOR_LENGTH + i), (LPVOID)&temp, 1, NULL);
			if ((int)temp >= 65 && (int)temp <= 122 && !nameFound) {
				nameFound = true;
			}

			if (!nameFound) {
				return;
			}

			if ((int)temp == 0x20) {
				break;
			}
			else if ((int)temp != 0) {
				name.push_back(temp);
			}
			++i;
		}

		ReadProcessMemory(handle, (LPCVOID)(basePointer + FIRST_GLADIATOR + j*GLADIATOR_LENGTH + STRENGTH), (LPVOID)&strength, 2, NULL);
		ReadProcessMemory(handle, (LPCVOID)(basePointer + FIRST_GLADIATOR + j*GLADIATOR_LENGTH + SPEED), (LPVOID)&speed, 2, NULL);
		ReadProcessMemory(handle, (LPCVOID)(basePointer + FIRST_GLADIATOR + j*GLADIATOR_LENGTH + MAX_MANA), (LPVOID)&max_mana, 2, NULL);
		ReadProcessMemory(handle, (LPCVOID)(basePointer + FIRST_GLADIATOR + j*GLADIATOR_LENGTH + MAX_STAMINA), (LPVOID)&max_stamina, 2, NULL);
		ReadProcessMemory(handle, (LPCVOID)(basePointer + FIRST_GLADIATOR + j*GLADIATOR_LENGTH + STAMINA), (LPVOID)&stamina, 2, NULL);
		ReadProcessMemory(handle, (LPCVOID)(basePointer + FIRST_GLADIATOR + j*GLADIATOR_LENGTH + MANA), (LPVOID)&mana, 2, NULL);
		ReadProcessMemory(handle, (LPCVOID)(basePointer + FIRST_GLADIATOR + j*GLADIATOR_LENGTH + FIST_OFFENSE), (LPVOID)&fist_offense, 2, NULL);
		ReadProcessMemory(handle, (LPCVOID)(basePointer + FIRST_GLADIATOR + j*GLADIATOR_LENGTH + FIST_DEFENSE), (LPVOID)&fist_defense, 2, NULL);
		ReadProcessMemory(handle, (LPCVOID)(basePointer + FIRST_GLADIATOR + j*GLADIATOR_LENGTH + SPEAR_OFFENSE), (LPVOID)&spear_offense, 2, NULL);
		ReadProcessMemory(handle, (LPCVOID)(basePointer + FIRST_GLADIATOR + j*GLADIATOR_LENGTH + SPEAR_DEFENSE), (LPVOID)&spear_defense, 2, NULL);
		ReadProcessMemory(handle, (LPCVOID)(basePointer + FIRST_GLADIATOR + j*GLADIATOR_LENGTH + SWORD_OFFENSE), (LPVOID)&sword_offense, 2, NULL);
		ReadProcessMemory(handle, (LPCVOID)(basePointer + FIRST_GLADIATOR + j*GLADIATOR_LENGTH + SWORD_DEFENSE), (LPVOID)&sword_defense, 2, NULL);
		ReadProcessMemory(handle, (LPCVOID)(basePointer + FIRST_GLADIATOR + j*GLADIATOR_LENGTH + AXE_OFFENSE), (LPVOID)&axe_offense, 2, NULL);
		ReadProcessMemory(handle, (LPCVOID)(basePointer + FIRST_GLADIATOR + j*GLADIATOR_LENGTH + AXE_DEFENSE), (LPVOID)&axe_defense, 2, NULL);
		ReadProcessMemory(handle, (LPCVOID)(basePointer + FIRST_GLADIATOR + j*GLADIATOR_LENGTH + MACE_OFFENSE), (LPVOID)&mace_offense, 2, NULL);
		ReadProcessMemory(handle, (LPCVOID)(basePointer + FIRST_GLADIATOR + j*GLADIATOR_LENGTH + MACE_DEFENSE), (LPVOID)&mace_defense, 2, NULL);
		ReadProcessMemory(handle, (LPCVOID)(basePointer + FIRST_GLADIATOR + j*GLADIATOR_LENGTH + SALARY), (LPVOID)&salary, 2, NULL);

		gladiators.push_back(Gladiator(name, strength, speed, max_mana, mana, max_stamina, stamina, fist_offense, fist_defense,
			spear_offense, spear_defense, sword_offense, sword_defense, axe_offense, axe_defense, mace_offense,
			mace_defense, salary));

		++j;
	}

}

void writeMemoryShortInt(const HANDLE &handle, DWORD address, short int value) {
	WriteProcessMemory(handle, (LPVOID)address, (LPCVOID)&value, 2, NULL);
}

void writeMemoryInt(const HANDLE &handle, DWORD address, int value) {
	WriteProcessMemory(handle, (LPVOID)address, (LPCVOID)&value, 4, NULL);
}

int readMemory(const HANDLE &handle, DWORD address) {
	int value = 0;

	ReadProcessMemory(handle, (LPCVOID)address, (LPVOID)&value, 4, NULL);

	return value;
}