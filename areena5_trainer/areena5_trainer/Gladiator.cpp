#include "Gladiator.hh"



Gladiator::Gladiator(const std::string &name, int strength, short int speed, short int max_mana,
	short int mana, short int max_stamina, short int stamina, short int fist_offense,
	short int fist_defense, short int spear_offense, short int spear_defense,
	short int sword_offense, short int sword_defense, short int axe_offense,
	short int axe_defense, short int mace_offense, short int mace_defense,
	int salary) :
	name(name), strength(strength), speed(speed), max_mana(max_mana), mana(mana), max_stamina(max_stamina),
	stamina(stamina), fist_offense(fist_offense), fist_defense(fist_defense), spear_offense(spear_offense),
	spear_defense(spear_defense), sword_offense(sword_offense), sword_defense(sword_defense),
	axe_offense(axe_offense), axe_defense(axe_defense), mace_offense(mace_offense),
	mace_defense(mace_defense), salary(salary)
{
}

void Gladiator::printInfo() const {
	std::cout << "Name: " << name << std::endl;
	std::cout << "(0) Salary: " << salary << std::endl;
	std::cout << "(1) Stamina: " << stamina << "/" << max_stamina << std::endl;
	std::cout << "(2) Mana: " << mana << "/" << max_mana << std::endl;
	std::cout << "(3) Strength: " << strength << std::endl;
	std::cout << "(4) Speed: " << speed << std::endl;

	std::cout << "(5) Fist: " << "A:" << fist_offense << " D:" << fist_defense << std::endl;
	std::cout << "(6) Spear: " << "A:" << spear_offense << " D:" << spear_defense << std::endl;
	std::cout << "(7) Sword: " << "A:" << sword_offense << " D:" << sword_defense << std::endl;
	std::cout << "(8) Axe: " << "A:" << axe_offense << " D:" << axe_defense << std::endl;
	std::cout << "(9) Mace: " << "A:" << mace_offense << " D:" << mace_defense << std::endl;
}

void Gladiator::printName() const {
	std::cout << name << std::endl;
}

Gladiator::~Gladiator()
{
}
